package helpers

import "time"

func AToTime(data string) (res time.Time) {
	res, _ = time.Parse("01/02/2006 03:04:05 PM", data)
	return res
}
