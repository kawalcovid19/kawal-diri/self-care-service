package helpers

import (
	"time"
)

const (
	StatUnknown               = "Belum Diketahui"
	StatHealthy               = "Sehat"
	StatSelfQuarantine        = "Karantina Mandiri"
	StatIntensiveRest         = "Istirahat Intensif"
	StatNeedMedicalAttention  = "Perlu Tindak Lanjut Medis"
	StatUnknownI              = 1
	StatHealthyI              = 2
	StatSelfQuarantineI       = 3
	StatIntensiveRestI        = 4
	StatNeedMedicalAttentionI = 5
	EdgeLabelDiagnosedAs      = "diagnosed_as"
	VertexLabelPerson         = "person"
	VertexLabelPatient        = "patient"
	PersonPrefix              = "per-"
	PatientPrefix             = "pat-"
)

var (
	WIB, _ = time.LoadLocation("Asia/Jakarta")

	StatusAtoI = map[string]int{
		StatUnknown:              StatUnknownI,
		StatHealthy:              StatHealthyI,
		StatSelfQuarantine:       StatSelfQuarantineI,
		StatIntensiveRest:        StatIntensiveRestI,
		StatNeedMedicalAttention: StatNeedMedicalAttentionI,
	}

	StatusItoA = map[int]string{
		StatUnknownI:              StatUnknown,
		StatHealthyI:              StatHealthy,
		StatSelfQuarantineI:       StatSelfQuarantine,
		StatIntensiveRestI:        StatIntensiveRest,
		StatNeedMedicalAttentionI: StatNeedMedicalAttention,
	}
)
