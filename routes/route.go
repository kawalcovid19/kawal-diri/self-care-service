package routes

import (
	"gitlab.com/kawalcovid19/kawal-diri/self-care-service/packages/users"

	"github.com/gin-gonic/gin"
)

// Initialize routes
func Initialize(r *gin.Engine) {
	user := users.SetUserProvider()

	api := r.Group("")
	{
		api.GET("/users/:id", user.Handler.FindOne)
		api.PUT("/users/update-status", user.Handler.UpdateStatusOne)
	}
}
