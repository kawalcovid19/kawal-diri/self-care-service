FROM golang:1.13-alpine as builder
COPY . /self-care-service

WORKDIR /self-care-service
RUN go get -d -v
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM alpine:3.9
RUN apk --no-cache add ca-certificates

WORKDIR /root/
COPY --from=builder /self-care-service/ .

EXPOSE 8080
CMD ["./app"]
