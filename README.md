# Self Care Service

## About
*User Story*
* As a kawal diri user, I want to take self diagnose survey so I know my current risk of getting COVID-19.
* As a kawal diri user, I want to track clinical suspected cases in Indonesia who claimed by hospital.
* As a kawal diri user, I want to possibly update my status.

## How to Run it?
### Prerequisites
* Go 1.13.x or later with go mod

```bash
$ go run main.go
```
