package users

import "time"

type Users struct {
	ID           uint      `json:"id"`
	FirstName    string    `json:"first_name"`
	LastName     string    `json:"last_name"`
	Gender       string    `json:"gender"`
	Status       string    `json:"status"`
	DateOfBirth  time.Time `json:"date_of_birth"`
	DiagnoseTime time.Time `json:"diagnose_time"`
	CreatedAt    time.Time `json:"created_at"`
	UpdatedAt    time.Time `json:"updated_at"`
}

type Patients struct {
	ID            string    `json:"id,omitempty"`
	Label         string    `json:"label,omitempty"`
	Pk            string    `json:"pk,omitempty"`
	CreatedDate   time.Time `json:"createdDate,omitempty"`
	Nationality   string    `json:"nationality,omitempty"`
	Sex           string    `json:"sex,omitempty"`
	Province      string    `json:"province,omitempty"`
	CaseStatus    string    `json:"caseStatus,omitempty"`
	DeceasedDate  time.Time `json:"deceaseDate,omitempty"`
	DiagnosedDate time.Time `json:"diagnoseDate,omitempty"`
	UpdatedDate   time.Time `json:"updateDate,omitempty"`
}
