package users

import (
	"log"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/palantir/stacktrace"
	"gitlab.com/kawalcovid19/kawal-diri/self-care-service/boot"
	"gitlab.com/kawalcovid19/kawal-diri/self-care-service/helpers"
)

type IUserService interface {
	// @TODO adding method interface below here

	GetUser(c *gin.Context, id uint) (user Patients, err error)
	UpdateUserStatus(c *gin.Context, req UpdateStatusRequest) (patient Patients, err error)
}

func NewUserService(repository IUserRepository) *UserService {
	return &UserService{r: repository}
}

type UserService struct {
	r IUserRepository
}

// @TODO Add method of type UserService below here

func (s *UserService) GetUser(c *gin.Context, id uint) (user Patients, err error) {
	userDB := boot.GetGremlinDB(c)
	user, err = s.r.GetUser(userDB, id)
	if err != nil {
		log.Println(err)
		return Patients{}, stacktrace.Propagate(err, "Failed to upsert user status")
	}
	return
}

func (s *UserService) UpdateUserStatus(c *gin.Context, req UpdateStatusRequest) (patient Patients, err error) {
	statusI, exists := helpers.StatusAtoI[strings.Title(req.Status)]
	if !exists {
		return Patients{}, stacktrace.NewError("Invalid Status")
	}

	upsertParam := UpdatePatientStatusParam{
		ID:     req.ID,
		Status: strings.Title(req.Status),
	}
	userDB := boot.GetGremlinDB(c)
	patientUpsert, err := s.r.UpsertPatientStatus(userDB, upsertParam)
	if err != nil {
		return Patients{}, stacktrace.Propagate(err, "Failed to upsert user status")
	}

	insertParam := InsertStatusParam{
		ID:     req.ID,
		Status: statusI,
	}
	pgDB := boot.GetPostgresDB(c)
	patientInsert, err := s.r.InsertStatus(pgDB, insertParam)
	if err != nil {
		return Patients{}, stacktrace.Propagate(err, "Failed to insert user status")
	}
	if patientUpsert.ID != patientInsert.ID {
		return Patients{}, stacktrace.NewError("Patient data do not match")
	}

	patient = patientUpsert

	return patient, err
}
