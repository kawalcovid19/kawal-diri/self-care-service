package users

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type IUserHandler interface {
	// @TODO Add handlers below of this line
	FindOne(c *gin.Context)
	UpdateStatusOne(c *gin.Context)
}

func NewUserHandler(service IUserService) *UserHandler {
	return &UserHandler{s: service}
}

type UserHandler struct {
	s IUserService
}

// @TODO Add implementation methods of the interface below this line

func (h *UserHandler) FindOne(c *gin.Context) {
	var res interface{}
	id, err := strconv.Atoi(c.Params.ByName("id"))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error": "bad request",
		})
	}
	data, err := h.s.GetUser(c, uint(id))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"error": "internal server error",
		})
		return
	}
	if (Patients{}) != data {
		res = data
	} else {
		res = map[string]interface{}{}
	}

	c.JSON(http.StatusOK, gin.H{
		"data": res,
	})
	return
}

func (h *UserHandler) UpdateStatusOne(c *gin.Context) {
	req := UpdateStatusRequest{}
	err := c.BindJSON(&req)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error": "bad request",
		})
		return
	}

	patient, err := h.s.UpdateUserStatus(c, req)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"error": "internal server error",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": patient,
	})
	return
}
