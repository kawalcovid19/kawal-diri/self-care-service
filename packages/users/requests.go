package users

type UpdateStatusRequest struct {
	ID     int    `json:"id"`
	Status string `json:"status"`
}
