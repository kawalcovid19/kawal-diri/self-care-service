package users

import (
	"database/sql"
	"errors"
	"fmt"
	"time"

	"encoding/json"

	"github.com/go-gremlin/gremlin"
	"gitlab.com/kawalcovid19/kawal-diri/self-care-service/helpers"
)

type IUserRepository interface {
	GetUser(db *gremlin.Client, userId uint) (res Patients, err error)
	UpsertPatientStatus(db *gremlin.Client, param UpdatePatientStatusParam) (res Patients, err error)
	InsertStatus(db *sql.DB, data InsertStatusParam) (res Patients, err error)
}

func NewUserRepository(model Users) *UserRepository {
	return &UserRepository{model: model}
}

type UserRepository struct {
	model Users
}

func (r *UserRepository) GetUser(db *gremlin.Client, userId uint) (res Patients, err error) {
	var resultMap []map[string]interface{}
	patientID := fmt.Sprintf("%s%d", helpers.PatientPrefix, userId)
	query := fmt.Sprintf(`g.V().has(id, '%s').valueMap()`, patientID)
	result, err := db.ExecQuery(query)
	if err != nil {
		return res, err
	}
	err = json.Unmarshal(result, &resultMap)
	if len(resultMap) > 1 {
		return res, errors.New("Unintended multiple results")
	}
	if len(resultMap) == 0 {
		return res, err
	}
	if len(resultMap) > 0 {
		a := resultMap[0]
		res = Patients{
			ID:            a["pk"].([]interface{})[0].(string),
			Label:         helpers.VertexLabelPatient,
			Pk:            a["pk"].([]interface{})[0].(string),
			CaseStatus:    a["caseStatus"].([]interface{})[0].(string),
			CreatedDate:   helpers.AToTime(a["diagnosedDate"].([]interface{})[0].(string)),
			DiagnosedDate: helpers.AToTime(a["diagnosedDate"].([]interface{})[0].(string)),
			UpdatedDate:   helpers.AToTime(a["updatedDate"].([]interface{})[0].(string)),
		}
	}
	return res, err
}

type UpdatePatientStatusParam struct {
	ID     int
	Status string
}

func (r *UserRepository) UpsertPatientStatus(db *gremlin.Client, data UpdatePatientStatusParam) (res Patients, err error) {
	personID := fmt.Sprintf("%s%d", helpers.PersonPrefix, data.ID)
	patientID := fmt.Sprintf("%s%d", helpers.PatientPrefix, data.ID)
	edgeID := fmt.Sprintf("%s-%s", personID, patientID)
	now := time.Now()
	nowStr := now.Format("01/02/2006 03:04:05 PM")
	query := fmt.Sprintf(`
		g.V('%s').
		outE('%s').
		fold().
		coalesce(
			unfold(), 
			g.V('%s').
			as('per').
			addV('%s').
				property('id', '%s').
				property('pk', '%s').
				property('caseStatus', '%s').
				property('diagnosedDate', '%s').
				property('updatedDate', '%s').
			as('pat').
			addE('%s').
				property('id', '%s').
				property('createdDate', '%s').
			from('per')
		).
		inV().
		hasLabel('%s').
		property('caseStatus', '%s').
		valueMap()`,
		personID,
		helpers.EdgeLabelDiagnosedAs,
		personID,
		helpers.VertexLabelPatient,
		patientID,
		patientID,
		data.Status,
		nowStr,
		nowStr,
		helpers.EdgeLabelDiagnosedAs,
		edgeID,
		nowStr,
		helpers.VertexLabelPatient,
		data.Status)

	result, err := db.ExecQuery(query)
	if err != nil {
		return res, err
	}
	resultMap := []map[string]interface{}{}
	err = json.Unmarshal(result, &resultMap)
	if len(resultMap) > 1 {
		return res, errors.New("Unintended multiple results")
	}
	if len(resultMap) == 0 {
		return res, err
	}
	a := resultMap[0]

	res = Patients{
		ID:            a["pk"].([]interface{})[0].(string),
		Label:         helpers.VertexLabelPatient,
		Pk:            a["pk"].([]interface{})[0].(string),
		CaseStatus:    a["caseStatus"].([]interface{})[0].(string),
		CreatedDate:   helpers.AToTime(a["diagnosedDate"].([]interface{})[0].(string)),
		DiagnosedDate: helpers.AToTime(a["diagnosedDate"].([]interface{})[0].(string)),
		UpdatedDate:   helpers.AToTime(a["updatedDate"].([]interface{})[0].(string)),
	}

	return res, err
}

type InsertStatusParam struct {
	ID     int
	Status int
}

func (r *UserRepository) InsertStatus(db *sql.DB, data InsertStatusParam) (res Patients, err error) {
	var (
		idI     int
		statusI int
	)

	query := `
	INSERT INTO "user_covid_histories" ("user_id", "covid_status", "created_at", "updated_at")
	VALUES ($1, $2, NOW(), NOW())
	RETURNING "user_id", "covid_status", "created_at", "updated_at"
	`

	err = db.QueryRow(query, data.ID, data.Status).Scan(&idI, &statusI, &res.DiagnosedDate, &res.UpdatedDate)
	if err != nil {
		return Patients{}, err
	}

	res.ID = fmt.Sprintf("%s%d", helpers.PatientPrefix, idI)
	res.Pk = fmt.Sprintf("%s%d", helpers.PatientPrefix, idI)
	res.CaseStatus = helpers.StatusItoA[statusI]

	return res, err
}
