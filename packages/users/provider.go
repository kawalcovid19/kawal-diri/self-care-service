package users

type UserProvider struct {
	Handler IUserHandler
}

func SetUserProvider() *UserProvider {
	model := Users{}
	repository := NewUserRepository(model)
	service := NewUserService(repository)
	handler := NewUserHandler(service)

	return &UserProvider{Handler: handler}
}
