package middlewares

import (
	"database/sql"

	"github.com/gin-gonic/gin"
	"github.com/go-gremlin/gremlin"
)

func SetUserDBContext(db *gremlin.Client) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("UserDB", db)
		c.Next()
	}
}

func SetPostgreDBContext(db *sql.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("PostgreDB", db)
		c.Next()
	}
}
