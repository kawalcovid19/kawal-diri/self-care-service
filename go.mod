module gitlab.com/kawalcovid19/kawal-diri/self-care-service

go 1.13

require (
	github.com/gin-gonic/gin v1.6.1
	github.com/go-gremlin/gremlin v0.0.0-20180314122959-0036b9fca17f
	github.com/lib/pq v1.3.0
	github.com/northwesternmutual/grammes v1.1.2
	github.com/palantir/stacktrace v0.0.0-20161112013806-78658fd2d177
	github.com/qasaur/gremgo v0.0.0-20191109200528-a609650a8a45 // indirect
	github.com/satori/go.uuid v1.2.1-0.20181028125025-b2ce2384e17b // indirect
	github.com/sirupsen/logrus v1.5.0
	github.com/spf13/viper v1.6.2
	google.golang.org/appengine v1.1.0
	honnef.co/go/tools v0.0.1-2020.1.3 // indirect
)
