package main

import (
	"flag"
	"fmt"

	"gitlab.com/kawalcovid19/kawal-diri/self-care-service/config"
	"gitlab.com/kawalcovid19/kawal-diri/self-care-service/server"
)

func main() {
	configFile := flag.String("conf", "config.yaml", "Set custom config path")
	flag.Parse()

	s := server.Setup()
	host, err := config.FindConfigValue(*configFile, "app.host")
	if err != nil {
		errorMsg := fmt.Sprint("Error when reading host", err.Error())
		fmt.Println(errorMsg)
	}

	fmt.Println("Self care service is online!")
	s.Run(host)
}
