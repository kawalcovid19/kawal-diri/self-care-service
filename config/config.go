package config

import (
	"github.com/palantir/stacktrace"
	"github.com/spf13/viper"
)

func FindConfigValue(path, key string) (value string, err error) {
	viper.SetConfigType("yaml")
	viper.SetConfigFile(path)

	if err := viper.ReadInConfig(); err != nil {
		return value, stacktrace.Propagate(err, "Failed to load config %s", key)
	}

	value = viper.GetString(key)
	return value, err
}
