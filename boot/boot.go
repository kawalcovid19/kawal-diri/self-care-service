package boot

import (
	"database/sql"
	"fmt"
	"io"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-gremlin/gremlin"
	"github.com/palantir/stacktrace"
	"github.com/sirupsen/logrus"
	"gitlab.com/kawalcovid19/kawal-diri/self-care-service/config"

	_ "github.com/lib/pq"
)

func LoadUserDB(configFile string) (db *gremlin.Client, err error) {
	host, err := config.FindConfigValue(configFile, "database.user-db.host")
	if err != nil {
		return db, stacktrace.Propagate(err, "Failed to load user db host")
	}

	username, err := config.FindConfigValue(configFile, "database.user-db.username")
	if err != nil {
		return db, stacktrace.Propagate(err, "Failed to load user db username")
	}

	key, err := config.FindConfigValue(configFile, "database.user-db.key")
	if err != nil {
		return db, stacktrace.Propagate(err, "Failed to load user db key")
	}

	auth := gremlin.OptAuthUserPass(username, key)
	db, err = gremlin.NewClient(host, auth)

	return db, err
}

func LoadPostgreDB(configFile string) (db *sql.DB, err error) {
	host, err := config.FindConfigValue(configFile, "database.postgresql.host")
	if err != nil {
		return db, stacktrace.Propagate(err, "Failed to load pgsql host")
	}

	port, err := config.FindConfigValue(configFile, "database.postgresql.port")
	if err != nil {
		return db, stacktrace.Propagate(err, "Failed to load pgsql port")
	}

	username, err := config.FindConfigValue(configFile, "database.postgresql.username")
	if err != nil {
		return db, stacktrace.Propagate(err, "Failed to load user pgsql username")
	}

	password, err := config.FindConfigValue(configFile, "database.postgresql.password")
	if err != nil {
		return db, stacktrace.Propagate(err, "Failed to load pgsql password")
	}

	name, err := config.FindConfigValue(configFile, "database.postgresql.name")
	if err != nil {
		return db, stacktrace.Propagate(err, "Failed to load pgsql db name")
	}

	sslMode, err := config.FindConfigValue(configFile, "database.postgresql.ssl_mode")
	if err != nil {
		return db, stacktrace.Propagate(err, "Failed to load pgsql ssl mode")
	}

	sslCert, err := config.FindConfigValue(configFile, "database.postgresql.ssl_cert")
	if err != nil {
		return db, stacktrace.Propagate(err, "Failed to load pgsql ssl cert")
	}

	sslKey, err := config.FindConfigValue(configFile, "database.postgresql.ssl_key")
	if err != nil {
		return db, stacktrace.Propagate(err, "Failed to load pgsql ssl key")
	}

	sslRootCert, err := config.FindConfigValue(configFile, "database.postgresql.ssl_root_cert")
	if err != nil {
		return db, stacktrace.Propagate(err, "Failed to load pgsql ssl root cert")
	}

	loc := time.UTC

	connStr := fmt.Sprintf(
		"postgres://%s:%s@%s:%s/%s?sslmode=%s&TimeZone=%s",
		username, password, host, port, name, sslMode, loc.String(),
	)

	if sslMode == "require" {
		connStr = fmt.Sprintf(
			"postgres://%s:%s@%s:%s/%s?sslmode=%s&TimeZone=UTC&sslcert=%s&sslkey=%s&sslrootcert=%s",
			username, password, host, port, name, sslMode, sslCert, sslKey, sslRootCert,
		)
	}

	db, err = sql.Open("postgres", connStr)
	if err != nil {
		return db, stacktrace.Propagate(err, "Failed to open connection to pgsql db")
	}
	return db, err
}

func GetPostgresDB(c *gin.Context) *sql.DB {
	return c.MustGet("PostgreDB").(*sql.DB)
}

func GetGremlinDB(c *gin.Context) *gremlin.Client {
	return c.MustGet("UserDB").(*gremlin.Client)
}

func LoadLogger(configFile string) (logger io.Writer, err error) {
	logger = os.Stdout
	logFile, err := config.FindConfigValue(configFile, "log.file_path")
	if err != nil {
		return nil, stacktrace.Propagate(err, "Failed to load log file path")
	}

	if logFile != "" {
		writer, err := os.Open(logFile)
		if err != nil {
			return nil, stacktrace.Propagate(err, "Failed to load log file")
		}
		lrus := logrus.New()
		lrus.SetOutput(writer)
		return lrus.Writer(), err
	}

	return logger, err
}
