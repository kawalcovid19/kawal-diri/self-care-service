package server

import (
	"fmt"
	"io"

	"github.com/gin-gonic/gin"
	"gitlab.com/kawalcovid19/kawal-diri/self-care-service/boot"
	"gitlab.com/kawalcovid19/kawal-diri/self-care-service/middlewares"
	"gitlab.com/kawalcovid19/kawal-diri/self-care-service/routes"
)

var (
	configFile = "./config.yaml"
)

func Setup() *gin.Engine {
	r := gin.Default()
	r.Use(gin.Recovery())

	logger, err := boot.LoadLogger(configFile)
	if err != nil {
		errorMsg := fmt.Sprintln("Error when loading logger: ", err.Error())
		fmt.Println(errorMsg)
	}
	r.Use(gin.LoggerWithWriter(logger))

	userDB, err := boot.LoadUserDB(configFile)
	if err != nil {
		errorMsg := fmt.Sprintln("Error when loading user database: ", err.Error())
		io.WriteString(logger, errorMsg)
	}
	if _, err := userDB.ExecQuery("g.V()"); err != nil {
		errorMsg := fmt.Sprintln("User DB is not connected: ", err.Error())
		io.WriteString(logger, errorMsg)
	} else {
		msg := fmt.Sprintln("User DB is connected")
		io.WriteString(logger, msg)
	}
	userDBMiddleware := middlewares.SetUserDBContext(userDB)
	r.Use(userDBMiddleware)

	postgreDB, err := boot.LoadPostgreDB(configFile)
	if err != nil {
		errorMsg := fmt.Sprintln("Error when loading user database: ", err.Error())
		io.WriteString(logger, errorMsg)
	}
	if err := postgreDB.Ping(); err != nil {
		errorMsg := fmt.Sprintln("Postgre DB failed to connect: ", err.Error())
		io.WriteString(logger, errorMsg)
	} else {
		msg := fmt.Sprintln("Postgre DB is connected")
		io.WriteString(logger, msg)
	}
	postgreDBMiddleware := middlewares.SetPostgreDBContext(postgreDB)
	r.Use(postgreDBMiddleware)

	routes.Initialize(r)
	return r
}
